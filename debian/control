Source: libvshadow
Priority: optional
Maintainer: Debian Security Tools <team+pkg-security@tracker.debian.org>
Uploaders: Hilko Bengen <bengen@debian.org>
Build-Depends: dpkg-dev (>= 1.22.5), debhelper-compat (= 11),
 dh-python,
 pkgconf,
 libfuse-dev,
 python3-dev, python3-setuptools,
Standards-Version: 4.6.1
Section: libs
Homepage: https://github.com/libyal/libvshadow
Vcs-Git: https://salsa.debian.org/pkg-security-team/libvshadow.git
Vcs-Browser: https://salsa.debian.org/pkg-security-team/libvshadow

Package: libvshadow-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends}, ${misc:Depends},
 libvshadow1t64 (= ${binary:Version})
Description: Volume Shadow Snapshot format access library -- development files
 libvshadow is a library to access the Volume Shadow Snapshot (VSS)
 format. The VSS format is used by Windows, as of Vista, to maintain
 copies of data on a storage media volume.
 .
 This package includes the development support files.

Package: libvshadow1t64
Provides: ${t64:Provides}
Replaces: libvshadow1
Breaks: libvshadow1 (<< ${source:Version})
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Volume Shadow Snapshot format access library
 libvshadow is a library to access the Volume Shadow Snapshot (VSS)
 format. The VSS format is used by Windows, as of Vista, to maintain
 copies of data on a storage media volume.
 .
 This package contains the shared library.

Package: libvshadow-utils
Section: otherosfs
Architecture: any
Multi-Arch: foreign
Depends: ${shlibs:Depends}, ${misc:Depends},
 libvshadow1t64,
Description: Volume Shadow Snapshot format access library -- Utilities
 libvshadow is a library to access the Volume Shadow Snapshot (VSS)
 format. The VSS format is used by Windows, as of Vista, to maintain
 copies of data on a storage media volume.
 .
 This package contains tools to access data stored in a Volume Shadow
 Snapshot: vshadowinfo, vshadowmount.

Package: python3-libvshadow
Section: python
Architecture: any
Depends: libvshadow1t64 (= ${binary:Version}), ${shlibs:Depends}, ${misc:Depends}, ${python3:Depends},
 ${python3:Depends},
Multi-Arch: same
Description: Volume Shadow Snapshot format access library -- Python 3 bindings
 libvshadow is a library to access the Volume Shadow Snapshot (VSS)
 format. The VSS format is used by Windows, as of Vista, to maintain
 copies of data on a storage media volume.
 .
 This package contains Python 3 bindings for libvshadow.
